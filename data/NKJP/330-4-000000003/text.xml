<?xml version="1.0" encoding="UTF-8"?>
<teiCorpus xmlns:xi="http://www.w3.org/2001/XInclude" xmlns="http://www.tei-c.org/ns/1.0">
 <xi:include href="NKJP_1M_header.xml"/>
 <TEI>
  <xi:include href="header.xml"/>
  <text xml:id="txt_text" xml:lang="pl">
   <body xml:id="txt_body">
    <div xml:id="txt_1-div" decls="#h_1-bibl">
     <ab n="p49in60of:IJP:WAB:SokolDuchowosc" xml:id="txt_1.1-ab">Gdy przychodzą ból i cierpienie, nieodłącznie przecież związane z życiem, często jedyne schronienie i ukojenie daje wymiar duchowy – owo inne spojrzenie, inna perspektywa, zdolna nadać nowy sens temu, co się wydarza i co się wydarzyło kiedyś.</ab>
     <ab n="p50in61of:IJP:WAB:SokolDuchowosc" xml:id="txt_1.2-ab">Z duchowością przestajemy częściej, niż się nam wydaje. Warto więc uwrażliwiać się na jej dostrzeganie w sobie, w innych, w tym, co się nam przytrafia.</ab>
    </div>
    <div xml:id="txt_2-div" decls="#h_2-bibl">
     <ab n="p483in526of:IJP:WAB:SokolDuchowosc" xml:id="txt_2.1-ab">Ktoś, kto stracił bezpowrotnie słuch w jednym uchu, może o sobie powiedzieć z uśmiechem, że jest najlepiej słyszącym na jedno ucho, lub uznać, że jego życie zostało złamane, i cierpieć z powodu straconych szans.</ab>
     <ab n="p484in527of:IJP:WAB:SokolDuchowosc" xml:id="txt_2.2-ab">Psychologia buddyjska stwierdza, że podstawową przyczyną ludzkich cierpień są nasze pragnienia i oczekiwania, a właściwie przywiązanie do nich, do wyobrażeń, jakie się z nimi wiążą, bo wtedy rozczarowania są nieuniknione.</ab>
    </div>
    <div xml:id="txt_3-div" decls="#h_3-bibl">
     <ab n="p485in528of:IJP:WAB:SokolDuchowosc" xml:id="txt_3.1-ab">Następną przyczyną zwiększania cierpienia jest bunt, opór, wywołujący żal i rozgoryczenie – dlaczego mnie się to zdarzyło? Wszystko to zwiększa ból i koncentruje uwagę na stracie, na tym, czego już nie możemy zmienić. Zasklepiamy się w pozycji skrzywdzonego, dotkniętego nieszczęściem.</ab>
     <ab n="p486in529of:IJP:WAB:SokolDuchowosc" xml:id="txt_3.2-ab">Jest naturalne, że staramy się unikać bólu i cierpienia. Jest też faktem, że mamy wpływ na rozmiar ich doznawania, jeżeli już się zdarzą, choć nie zawsze zdajemy sobie z tego sprawę.</ab>
    </div>
    <div xml:id="txt_4-div" decls="#h_4-bibl">
     <ab n="p436in479of:IJP:WAB:SokolDuchowosc" xml:id="txt_4.1-ab">Nie zawsze mamy możliwość dokonania takiego eksperymentu w całości, ale nawet ograniczenie się do gry wyobraźni i zadanie sobie pytania: co bym zrobił, gdybym miał przed sobą rok życia, a następnie zapisanie tego, co serce dyktuje, mogłoby się stać okazją do sprawdzenia, czy potrzebne są nam jakieś zmiany w bieżącym życiu.</ab>
    </div>
    <div xml:id="txt_5-div" decls="#h_5-bibl">
     <ab n="p51in62of:IJP:WAB:SokolDuchowosc" xml:id="txt_5.1-ab">Mogą temu posłużyć poniższe pytania:</ab>
     <ab n="p52in64of:IJP:WAB:SokolDuchowosc" xml:id="txt_5.2-ab">Czy znasz jakieś osoby, które nazwałbyś autorytetami duchowymi? Albo takie, które promieniują czymś szczególnym? Co je charakteryzuje? Co Cię w nich pociąga?</ab>
     <ab n="p53in65of:IJP:WAB:SokolDuchowosc" xml:id="txt_5.3-ab">Przypomnij sobie własne doświadczenia, które mógłbyś przypisać do wymiaru duchowego. Mogą to być zdarzenia, które odczytałeś jako specjalne znaki dla siebie; chwile szczególnego stanu świadomości, w którym coś się unaoczniło; momenty specjalnej radości, gdy udało Ci się przekroczyć własne ograniczenia.</ab>
    </div>
    <div xml:id="txt_6-div" decls="#h_6-bibl">
     <ab n="p104in116of:IJP:WAB:SokolDuchowosc" xml:id="txt_6.1-ab">I odwrotnie, zwiększanie udziału świadomości w naszym życiu (a jest to proces praktycznie bez końca) może wnosić coraz to nowe i nieoczekiwane bogactwa, co nie oznacza, że efekty przyjdą bez wysiłku i bólu.</ab>
     <ab n="p105in117of:IJP:WAB:SokolDuchowosc" xml:id="txt_6.2-ab">W następnych rozdziałach spróbuję pokazać, jak na różne sposoby zwiększać świadomość własnego życia, a tymczasem zapraszam do własnej refleksji:</ab>
     <ab n="p106in118of:IJP:WAB:SokolDuchowosc" xml:id="txt_6.3-ab">Co w Twoim sposobie życia wydaje Ci się największym zagrożeniem, co Cię destabilizuje, wnosi niepokój?</ab>
    </div>
    <div xml:id="txt_7-div" decls="#h_7-bibl">
     <ab n="p445in488of:IJP:WAB:SokolDuchowosc" xml:id="txt_7.1-ab">Spałbym mało, śniłbym więcej. Wiem, że w każdej minucie z zamkniętymi oczami tracimy 60 sekund światła.</ab>
     <ab n="p446in489of:IJP:WAB:SokolDuchowosc" xml:id="txt_7.2-ab">Szedłbym, kiedy inni się zatrzymują.</ab>
     <ab n="p447in490of:IJP:WAB:SokolDuchowosc" xml:id="txt_7.3-ab">Budziłbym się, kiedy inni śpią.</ab>
     <ab n="p448in491of:IJP:WAB:SokolDuchowosc" xml:id="txt_7.4-ab">Gdyby Bóg podarował mi odrobinę życia, ubrałbym się prosto, rzuciłbym się ku słońcu, odkrywając nie tylko moje ciało, ale i moją duszę.</ab>
    </div>
   </body>
  </text>
 </TEI>
</teiCorpus>
