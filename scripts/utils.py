#!/usr/bin/env python3
import numpy as np


def pad_nested_sequences(sequences, max_sent_len=None, max_word_len=None, dtype='int32'):
    """Pads nested sequences to the same length.
    Args:
        sequences: List of lists of lists.
        dtype: Type of the output sequences.
    # Returns
        x: Numpy array of shape `(num_samples, max_sent_len, max_word_len)`
    """
    if max_word_len is None and max_sent_len is None:
        max_sent_len = 0
        max_word_len = 0
        for sent in sequences:
            max_sent_len = max(len(sent), max_sent_len)
            for word in sent:
                max_word_len = max(len(word), max_word_len)

    x = np.zeros((len(sequences), max_sent_len, max_word_len)).astype(dtype)
    for i, sent in enumerate(sequences):
        for j, word in enumerate(sent):
            if j == max_sent_len:
                break
            x[i, j, :len(word)] = word[:max_word_len]

    return x


def filter_embeddings(embeddings, vocab, dim):
    """Loads word vectors in numpy array.
    Args:
        embeddings (dict): a dictionary of numpy array.
        vocab (dict): word_index lookup table.
    Returns:
        numpy array: an array of word embeddings.
    """
    not_found = 0
    found = 0
    _embeddings = np.zeros([len(vocab), dim])
    for word in vocab:
        try:
            emb = embeddings.get_vector(word)
            word_idx = vocab[word]
            _embeddings[word_idx] = emb
            found = found +1
        except:
          not_found = not_found + 1
    print(not_found)
    print(found)
    return _embeddings