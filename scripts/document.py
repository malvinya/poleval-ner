import pandas as pd
import csv
import matplotlib.pyplot as plt


class Document(object):

    def __init__(self, entry_text, filename):
        self.entry_text = entry_text
        self.data = pd.read_csv(filename, quotechar='"', sep=',',
                                quoting=csv.QUOTE_NONNUMERIC, keep_default_na=False)
        self.n_sent = len(self.data.groupby(['Sentence#']))
        self.empty = False
        self.data['SentenceId'] = self.data['Sentence#'].apply(lambda x: int(x.split(':')[1]))
        agg_func = lambda s: [w for w in s["Word"].values.tolist()]
        # agg_func = lambda s: [(w, t) for w, t in zip(s["Word"].values.tolist(),
        # s["CTag"].values.tolist())]
        self.grouped = self.data.groupby("SentenceId").apply(agg_func)
        self.sentences = [s for s in self.grouped]

    def get_sentences(self):
        return self.sentences

    def get_tokenized_content(self):
        return self.data['Word'].tolist()

    def get_raw_content(self):
        return self.entry_text

    def align_tokens_to_text(self):
        self.offsets = {}
        tid = 0
        tokens = self.get_tokenized_content()
        for t_index, t in enumerate(tokens):
            start = self.entry_text.find(str(t), tid)
            if start == -1:
                raise Exception("Could not align tokens to text")
            end = start + len(t)
            self.offsets[t_index] = (start, end)
            tid = end

    def get_offset_for_token(self, token_idx):
        return self.offsets[token_idx]

    def plot_sentence_by_length(self):
        # Plot sentence by lenght
        plt.hist([len(s) for s in self.sentences], bins=50)
        plt.title('Token per sentence')
        plt.xlabel('Len (number of token)')
        plt.ylabel('# samples')
        plt.show()


if __name__ == "__main__":
    document = Document("ala ma kota", "../data/ala_ma_kota.txt")
    content = document.get_sentences()
    print(len(content))
    document.align_tokens_to_text()
    print(document.get_offset_for_token(1))
    print(document.get_tokenized_content())
