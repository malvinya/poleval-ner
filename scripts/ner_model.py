#!/usr/bin/env python3
from keras.layers import Bidirectional, concatenate, SpatialDropout1D
from keras.layers import LSTM, Embedding, Dense, TimeDistributed
from keras.models import Model, Input
from keras_contrib.layers import CRF
from keras_contrib.utils import save_load_utils
from keras.models import load_model

from scripts.index_transformer import IndexTransformer


class NerModel(object):
    def __init__(self,
                 char_vocab_size,
                 word_vocab_size,
                 n_tags,
                 char_embedding_dim=25,
                 word_embedding_dim=100,
                 char_lstm_size=25,
                 word_lstm_size=100,
                 max_len=140,
                 max_len_char=75,
                 embeddings=None,
                 use_crf=False
                 ):
        self.word_vocab_size = word_vocab_size
        self.char_vocab_size = char_vocab_size
        self.n_tags = n_tags
        self.max_len = max_len
        self.max_len_char = max_len_char
        self.word_embedding_dim = word_embedding_dim
        self.char_embedding_dim = char_embedding_dim
        self.word_lstm_size = word_lstm_size
        self.char_lstm_size = char_lstm_size
        self.use_embeddings = embeddings is not None
        self.embeddings = embeddings
        self.use_crf = use_crf
        self._keras_model, self._loss, self._metrics = self.build()

    def build(self):
        word_in = Input(shape=(self.max_len,))
        if not self.use_embeddings:
            word_embeddings = Embedding(input_dim=self.word_vocab_size,
                                        output_dim=self.word_embedding_dim,
                                        mask_zero=True,
                                        input_length=self.max_len,
                                        name='word_embedding')(word_in)
        else:
            word_embeddings = Embedding(input_dim=self.embeddings.shape[0],
                                        output_dim=self.embeddings.shape[1],
                                        mask_zero=True,
                                        weights=[self.embeddings],
                                        name='word_embedding')(word_in)

        char_in = Input(shape=(self.max_len, self.max_len_char,))
        char_embeddings = TimeDistributed(Embedding(input_dim=self.char_vocab_size,
                                                    output_dim=self.char_embedding_dim,
                                                    input_length=self.max_len_char,
                                                    mask_zero=True))(char_in)
        char_encodings = TimeDistributed(Bidirectional(LSTM(units=self.char_lstm_size)))(char_embeddings)

        # main LSTM
        x = concatenate([word_embeddings, char_encodings])
        x = SpatialDropout1D(0.3)(x)
        main_lstm = Bidirectional(LSTM(units=self.word_lstm_size, return_sequences=True,
                                       recurrent_dropout=0.6))(x)
        main_lstm = Dense(100, activation='tanh')(main_lstm)
        if self.use_crf:
            crf = CRF(self.n_tags)
            loss = crf.loss_function
            out = crf(main_lstm)
            metrics = [crf.accuracy]
        else:
            loss = 'categorical_crossentropy'
            out = Dense(self.n_tags, activation='softmax')(main_lstm)
            metrics = ["acc"]
        # else:
        # loss = 'categorical_crossentropy'
        # out = TimeDistributed(Dense(n_tags + 1, activation="softmax"))(main_lstm)
        # metrics=["acc"]
        return Model([word_in, char_in], out), loss, metrics

    @property
    def model(self):
        return self._keras_model

    @property
    def loss(self):
        return self._loss

    @property
    def metrics(self):
        return self._metrics

    def load_from_file(self, filename):
        return save_load_utils.load_all_weights(self._keras_model, filename)


def load_non_crf_model(model_filename):
    return load_model(model_filename)


def load_crf_model(model_filename,
                   index_transformer_filename,
                   char_embedding_dim=25,
                   word_embedding_dim=100,
                   char_lstm_size=25,
                   word_lstm_size=100,
                   max_len=140,
                   max_len_char=75):
    transformer = IndexTransformer.load(index_transformer_filename)

    nermodel = NerModel(char_vocab_size=transformer.char_vocab_size,
                        word_vocab_size=transformer.word_vocab_size,
                        n_tags=transformer.label_size,
                        char_embedding_dim=char_embedding_dim,
                        word_embedding_dim=word_embedding_dim,
                        char_lstm_size=char_lstm_size,
                        word_lstm_size=word_lstm_size,
                        max_len=max_len,
                        max_len_char=max_len_char,
                        use_crf=True)
    model = nermodel.model
    model.summary()
    nermodel.load_from_file(model_filename)
    return nermodel.model


if __name__ == "__main__":
    load_crf_model()
