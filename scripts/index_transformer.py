#!/usr/bin/env python3
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from keras.preprocessing.sequence import pad_sequences
from keras.utils.np_utils import to_categorical
from sklearn.externals import joblib

from scripts.utils import pad_nested_sequences
from scripts.vocabulary import Vocabulary


class IndexTransformer(BaseEstimator, TransformerMixin):
    def __init__(self, lower=True, num_norm=True,
                 use_char=True, initial_vocab=None, max_sent_len=0, max_word_len=0):
        self._num_norm = num_norm
        self._use_char = use_char
        self._word_vocab = Vocabulary(lower=lower)
        self._char_vocab = Vocabulary(lower=False)
        self._label_vocab = Vocabulary(lower=False, unk_token=False)
        self._max_sent_len = max_sent_len
        self._max_word_len = max_word_len
        if initial_vocab:
            self._word_vocab.add_documents([initial_vocab])
            self._char_vocab.add_documents(initial_vocab)  # TODO  remove initial vocab

    def fit(self, X, y):
        self._word_vocab.add_documents(X)
        self._label_vocab.add_documents(y)
        if self._use_char:
            for doc in X:
                self._char_vocab.add_documents(doc)

        self._word_vocab.build()
        self._char_vocab.build()
        self._label_vocab.build()
        if self._max_sent_len == 0 or self._max_word_len == 0:
            for sent in X:
                self._max_sent_len = max(len(sent), self._max_sent_len)
                for word in sent:
                    self._max_word_len = max(len(word), self._max_word_len)
        return self

    def transform(self, X, y=None):
        word_ids = [self._word_vocab.doc2id(doc) for doc in X]
        word_ids = pad_sequences(word_ids, maxlen=self._max_sent_len, padding='post', truncating='post')

        if self._use_char:
            char_ids = [[self._char_vocab.doc2id(w) for w in doc] for doc in X]
            char_ids = pad_nested_sequences(char_ids, self._max_sent_len, self._max_word_len)
            features = [word_ids, char_ids]
        else:
            features = word_ids

        if y is not None:
            y = [self._label_vocab.doc2id(doc) for doc in y]
            y = pad_sequences(y, maxlen=self._max_sent_len, padding='post', truncating='post')
            y = to_categorical(y, self.label_size).astype(int)
            # In 2018/06/01, to_categorical is a bit strange.
            # >>> to_categorical([[1,3]], num_classes=4).shape
            # (1, 2, 4)
            # >>> to_categorical([[1]], num_classes=4).shape
            # (1, 4)
            # So, I expand dimensions when len(y.shape) == 2.
            y = y if len(y.shape) == 3 else np.expand_dims(y, axis=0)
            return features, y
        else:
            return features

    def fit_transform(self, X, y=None, **params):
        return self.fit(X, y).transform(X, y)

    def inverse_transform(self, y, lengths=None):
        y = np.argmax(y, -1)
        inverse_y = [self._label_vocab.id2doc(ids) for ids in y]
        if lengths is not None:
            inverse_y = [iy[:l] for iy, l in zip(inverse_y, lengths)]

        return inverse_y

    @property
    def word_vocab_size(self):
        return len(self._word_vocab)

    @property
    def char_vocab_size(self):
        return len(self._char_vocab)

    @property
    def label_size(self):
        return len(self._label_vocab)

    def save(self, file_path):
        joblib.dump(self, file_path)

    @classmethod
    def load(cls, file_path):
        p = joblib.load(file_path)

        return p


if __name__ == "__main__":
    p = IndexTransformer(max_sent_len=6, max_word_len=11)
    p.fit([['ala', 'ma', 'kota', 'ladnego'], ['Ala', 'ma', 'psa']],
          [['0', 'B-orgName', '0'], ['B-persName_forename', '0', 'B-persName']])

    print(p.transform([['psa', 'ma', 'kota'], ['wiewiorka', 'ma', 'kota']]))
    print(p._max_sent_len)
    print(p._max_word_len)
