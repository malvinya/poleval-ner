import numpy as np


class NerTagger(object):

    def __init__(self, model, max_len, max_char_len):
        self.max_char_len = max_char_len
        self.max_len = max_len
        self.model = model

    def convert_to_labels(self, sentence, predicted):
        # predicted = np.argmax(predicted, axis=-1)
        labels = []
        for w, pred in zip(sentence, predicted):
            if w != 0:
                labels.append(pred)
        return labels

    def tag_sentences(self, word_vectors, char_vectors, transformer):
        output = []
        y_pred = self.model.predict([word_vectors,
                                     np.array(char_vectors).reshape((len(char_vectors),
                                                                     self.max_len, self.max_char_len))])
        yy_p = transformer.inverse_transform(y_pred)
        for si, sentence in enumerate(word_vectors):
            # reshaped_sentence = np.reshape(sentence,(1, 75))
            # predicted_label = self.model.predict([reshaped_sentence, np.array(char_vectors[si]).reshape((1, 75, 10))])
            labels = self.convert_to_labels(sentence, yy_p[si])
            output.append(labels)
        return output
