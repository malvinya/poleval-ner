#!/usr/bin/env python3

import json

import numpy as np

from scripts.document import Document
from scripts.index_transformer import IndexTransformer
from scripts.ner_tagger import NerTagger
from scripts.ner_model import load_crf_model, NerModel, load_non_crf_model

TEST_FILES = "../data/poleval_test_csv/text_%s.csv"
TEST_FILE = 'eval/poleval_test_ner_2018.json'

RESULT_FILE = '../results/results_crf_embeddings.json'
MODEL_PATH = "../models/model_crf_embeddings.h5"
INDEX_TRANSFORMER_PATH = "../models/index_transformer_crf_embeddings.pkl"
IS_CRF_MODEL = True

MAX_SENT_LEN = 140
MAX_WORD_LEN = 75


def group_labels_into_sequences(all_labels):
    # todo improve to handle I tags without B
    annotations = []
    closed_annotations = []
    for token_id, label in enumerate(all_labels):
        if (label == '0'):
            for a in annotations:
                closed_annotations.append(a)
            annotations = []
        for ann in label.split('#'):
            type = ann[2:]
            if 'B-' in ann:
                annotations.append({'type': type, 'token_ids': [token_id]})
            elif 'I-' in ann:
                found = False
                for _ann in reversed(annotations):
                    if type == _ann['type']:
                        _ann['token_ids'].append(token_id)
                        found = True
                        break
                if not found:
                    annotations.append({'type': type, 'token_ids': [token_id]})
    for a in annotations:
        closed_annotations.append(a)
    return closed_annotations


def align_prediction_to_text(document, predicted_labels):
    annotations = group_labels_into_sequences(predicted_labels)
    document.align_tokens_to_text()
    result = ''
    for an in annotations:
        begin = document.get_offset_for_token(an['token_ids'][0])[0]
        end = document.get_offset_for_token(an['token_ids'][-1])[1]
        orth = document.get_raw_content()[begin:end]
        result = result + ("%s %s %s\t%s\n" % (an['type'], begin, end, orth))
    return result


def save_results():
    with open(RESULT_FILE, 'w') as outfile:
        json.dump(data, outfile)


def print_predictions(document, all_labels):
    for w, t in zip(document.get_tokenized_content(), all_labels):
        print("{:15}: {:5}".format(w, t))


# TODO add early stopping
# TODO word form features like all uppercase,

if __name__ == "__main__":
    transformer = IndexTransformer.load(INDEX_TRANSFORMER_PATH)
    if IS_CRF_MODEL:
        model = load_crf_model(MODEL_PATH,
                               INDEX_TRANSFORMER_PATH,
                               char_embedding_dim=25,
                               word_embedding_dim=100,
                               char_lstm_size=25,
                               word_lstm_size=100,
                               max_len=MAX_SENT_LEN,
                               max_len_char=MAX_WORD_LEN)
    else:
        model = load_non_crf_model(MODEL_PATH)
    tagger = NerTagger(model, MAX_SENT_LEN, MAX_WORD_LEN)
    with open(TEST_FILE) as json_file:
        data = json.load(json_file)
        for entry_index, entry in enumerate(data):
            print(entry_index)
            document = Document(entry['text'], TEST_FILES % entry_index)
            sentences = document.get_sentences()
            word_vectors, char_vectors = transformer.transform(sentences)
            labels_by_sentence = tagger.tag_sentences(word_vectors, char_vectors, transformer)
            all_labels = np.concatenate(labels_by_sentence)
            entry['answers'] = align_prediction_to_text(document, all_labels)
            # print_predictions(document, all_labels)
            # print(entry['answers'])
        save_results()
