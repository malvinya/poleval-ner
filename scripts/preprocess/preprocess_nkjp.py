#!/usr/bin/env python3
import codecs
import glob
from lxml import etree
from collections import defaultdict
import os

MORPHO_SYNTAX_PATH = "../../data/NKJP/*/ann_morphosyntax.xml"

TRAIN_PATH = '../../data/train_input.csv'

LABELS = ['0', 'B-persName#B-persName_forename', 'B-orgName', 'I-orgName', 'B-placeName_country',
          'I-persName#B-persName_surname', 'B-placeName_settlement', 'B-persName#B-persName_surname', 'I-date',
          'B-date', 'B-geogName', 'I-geogName', 'I-time', 'B-persName', 'I-orgName#B-placeName_settlement',
          'B-placeName_region', 'B-persName#B-persName_addName', 'I-persName#I-persName_surname',
          'I-persName#I-persName_forename', 'I-persName', 'B-time', 'I-placeName_settlement', 'I-orgName#B-orgName',
          'B-placeName', 'I-persName#B-persName_forename', 'I-placeName_region', 'I-orgName#B-placeName_country',
          'B-placeName_district', 'I-placeName_country', 'I-geogName#B-placeName_settlement',
          'I-persName#I-persName_addName', 'I-orgName#B-geogName', 'I-geogName#B-persName#B-persName_surname',
          'I-placeName_region#B-placeName_settlement', 'B-orgName#B-placeName_country', 'B-date#I-date',
          'B-orgName#B-placeName_settlement', 'I-persName#B-persName_addName']


class Preprocessor(object):

    def allptrs(self, ptrs, named2ptrs):
        newptrs = list()
        for ptr in ptrs:
            if 'named_' not in ptr:
                newptrs.append(ptr)
            else:
                # Recursive call
                newptrs += self.allptrs(named2ptrs[ptr][1], named2ptrs)
        return newptrs

    def prepare_fnl(self, tree, nss):
        named2ptrs = defaultdict(list)
        for seg in tree.findall('.//seg', namespaces=nss):
            named = seg.xpath('./@xml:id')[0]
            for f in seg.findall('fs/f', namespaces=nss):
                if f.get('name') == 'type':
                    entity = f.getchildren()[0].get('value')
                elif f.get('name') == 'subtype':
                    entity += '_' + f.getchildren()[0].get('value')
            ptrs = [f.get('target').split('_')[-1] if 'named_' not in f.get('target') else f.get('target') for f in
                    seg.findall('ptr', namespaces=nss)]
            named2ptrs[named] = (entity, ptrs)
        fnl = list()
        for named in named2ptrs:
            entity, ptrs = named2ptrs[named]
            ptrs = self.allptrs(ptrs, named2ptrs)
            fnl.append((entity, ptrs))
        return fnl

    def preprocess(self):
        with codecs.open(TRAIN_PATH, 'w', encoding='utf8') as train:
            train.write('Sentence#,Word,Tag\n')
            sent_id = 0
            for file in glob.glob(MORPHO_SYNTAX_PATH):
                namedfile = file.replace('morphosyntax', 'named')
                if not os.path.exists(namedfile):
                    continue

                tree, nss = self.prepare_tree(namedfile)
                fnl = self.prepare_fnl(tree, nss)

                tree, nss = self.prepare_tree(file)
                for s in tree.findall('.//s', namespaces=nss):
                    text = []
                    ids = []
                    for seg in s:
                        ids.append(seg.get('corresp').split('_')[-1])
                        text += [(f.getchildren()[0].text, ['0']) for f in seg.findall('fs/f', namespaces=nss) if
                                 f.get('name') == 'orth']
                    for it, t in enumerate(text):
                        for entity, targets in fnl:
                            if len(targets) and (ids[it] in targets):
                                if (ids[it] == targets[0]):
                                    pref = 'B-'
                                elif ids[it - 1] not in targets:
                                    # print(f'disjoint entity{t}')
                                    pref = 'B-'
                                else:
                                    pref = 'I-'
                                if t[1] == ['0']:
                                    t[1].remove('0')
                                    t[1].append(f'{pref}{entity}')
                                else:
                                    if f'{pref}{entity}' not in t[1]:
                                        t[1].append(f'{pref}{entity}')
                    for w, e in text:
                        w = w.replace('"', '""');
                        tag = "#".join(str(x) for x in e)
                        if tag not in LABELS:
                            if '#' not in tag:
                                tag = '0'
                            else:
                                new_tag = tag[:tag.rfind('#')]
                                while new_tag not in LABELS:
                                    if '#' not in new_tag:
                                        new_tag = '0'
                                    else:
                                        new_tag = new_tag[:new_tag.rfind('#')]
                                tag = new_tag
                        train.write('"Sentence:{}","{}","{}"\n'.format(sent_id, w, tag))
                    sent_id = sent_id + 1

    @staticmethod
    def prepare_tree(filename):
        tree = etree.parse(filename)
        nss = tree.getroot().nsmap
        return tree, nss


if __name__ == "__main__":
    Preprocessor().preprocess()
