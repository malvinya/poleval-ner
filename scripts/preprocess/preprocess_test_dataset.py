#!/usr/bin/env python3

import glob
import os
import codecs
from lxml import etree

INPUT_DIRECTORY = '../../data/poleval_test_xml/*.xml'

OUTPUT_DIRECTORY = '../../data/poleval_test_csv/'

if __name__ == "__main__":
    for path in glob.glob(INPUT_DIRECTORY):
        filename = os.path.basename(path)
        output_filename = filename.replace("xml", "csv")
        with codecs.open(OUTPUT_DIRECTORY + output_filename, 'w', encoding='utf8') as output:
            output.write('Sentence#,Word,CTag\n')
            print(path)
            tree = etree.parse(path)
            nss = tree.getroot().nsmap
            for seg in tree.findall('.//sentence', namespaces=nss):
                for child in seg.findall('tok'):
                    sentence_id = seg.attrib['id'].replace('s','')
                    word = child.find('orth').text
                    ctag = child.find('lex/ctag').text
                    word = word.replace('"', '""')
                    output.write('"Sentence:{}","{}","{}"\n'.format(sentence_id, word, ctag))