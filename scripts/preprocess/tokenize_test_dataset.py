#!/usr/bin/env python3
import requests
import json

OUTPUT_FILENAME = "../../data/poleval_test/text_%s.xml"

CLARIN_URL = "http://ws.clarin-pl.eu/nlprest2//base/process"

if __name__ == "__main__":

    session = requests.Session()
    with open('eval/poleval_test_ner_2018.json') as json_file:
        data = json.load(json_file)
        for i,entry in enumerate(data):
            payload = {"lpmn": "any2txt|wcrft2",
                       "text": entry['text'],
                       "user": "s19901@pjwstk.edu.pl"
                       }
            r = session.post(CLARIN_URL, data=json.dumps(payload))
            response = r.text
            with open(OUTPUT_FILENAME % i, 'wb') as f:
                f.write(r.content)