#!/usr/bin/env python3

import pandas as pd
import csv

if __name__ == "__main__":
    data = pd.read_csv("../../data/train_all.csv", quotechar = '"',sep=',',keep_default_na=False,quoting=csv.QUOTE_NONNUMERIC)
    data = data.fillna(method="ffill")
    labels_below_20 = data.groupby(['Tag']).count().nlargest(38, columns='Word').index._data
    print(labels_below_20.tolist())
    # print(data.groupby(['Tag']).count().nlargest(41, columns='Word'))
    for index, row in data.iterrows():
        original_tag = row['Tag']
        if (original_tag in labels_below_20) and '#' in original_tag:
            new_tag = original_tag[:original_tag.rfind('#')]
            while new_tag in labels_below_20:
                    new_tag = new_tag[:new_tag.rfind('#')]
            row['Tag']=new_tag
    # data.to_csv("../data/train_all_truncated.csv", quotechar='"', sep=',', quoting=csv.QUOTE_NONNUMERIC)