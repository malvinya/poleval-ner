#!/usr/bin/env python3
from collections import Counter

PAD = '<pad>'

UNK = '<unk>'


class Vocabulary(object):
    """
        _token_count: A collections.Counter object holding the frequencies of tokens
            in the data used to build the Vocabulary.
        _token2id: A collections.defaultdict instance mapping token strings to
            numerical identifiers.
        _id2token: A list of token strings indexed by their numerical identifiers.
    """
    def __init__(self, max_size=None, lower=True, unk_token=True):
        self._max_size = max_size
        self._lower = lower
        self._unk = unk_token
        special_tokens = (PAD,)
        self._token2id = {token: i for i, token in enumerate(special_tokens)}
        self._id2token = list(special_tokens)
        self._token_count = Counter()

    def __len__(self):
        return len(self._token2id)

    def add_token(self, token):
        token = self.process_token(token)
        self._token_count.update([token])

    def process_token(self, token):
        if self._lower:
            token = token.lower()
        return token

    def add_documents(self, docs):
        for doc in docs:
            sent_tokens = map(self.process_token, doc)
            self._token_count.update(sent_tokens)

    def doc2id(self, doc):
        doc = map(self.process_token, doc)
        return [self.token_to_id(token) for token in doc]

    def id2doc(self, ids):
        return [self.id_to_token(idx) for idx in ids]

    def token_to_id(self, token):
        token = self.process_token(token)
        return self._token2id.get(token, self._token2id.get(UNK))

    def id_to_token(self, idx):
        return self._id2token[idx]

    def build(self):
        token_freq = self._token_count.most_common(self._max_size)
        idx = len(self.vocab)
        if self._unk:
            unk = UNK
            self._token2id[unk] = idx
            self._id2token.append(unk)
            idx +=1
        for token, _ in token_freq:
            self._token2id[token] = idx
            self._id2token.append(token)
            idx += 1


    @property
    def vocab(self):
        return self._token2id

    @property
    def reverse_vocab(self):
        return self._id2token


if __name__ == "__main__":
    word_vocab = Vocabulary()
    word_vocab.add_token("Ala")
    word_vocab.add_documents([['ala', 'ma', 'kota'], ['kot', 'nie', 'ma', 'mleka']])
    word_vocab.build()
    print(word_vocab.doc2id(['ala', 'nie', 'ma', 'mleka']))
    print(word_vocab.doc2id(['ala', 'nie', 'ma', 'psa']))
    print(word_vocab.id2doc([1, 5, 2, 6]))
    print(word_vocab.id2doc([1, 5, 2, 7]))
    print(word_vocab.token_to_id('<pad>'))
    print(word_vocab.token_to_id('<unk>'))
    print(word_vocab.id_to_token(1))
    print(word_vocab.id_to_token(0))
    print(word_vocab.token_to_id('nieznazny'))
    print(word_vocab.id_to_token(7))
    print(len(word_vocab))

    char_vocab = Vocabulary(lower=False)
    char_vocab.add_documents('Ala ma kota')
    char_vocab.build()
    print(len(char_vocab))
    print(char_vocab.doc2id('Ala'))
    print(char_vocab.id2doc([2,4,2,5]))

    l_vocab = Vocabulary(unk_token=False)
    l_vocab.add_documents(["B-pers"])
    l_vocab.build()
    print(l_vocab.token_to_id('<pad>'))