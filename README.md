# PolishNER - Project INL 
## Read more about the project
[Project Report in PDF](https://gitlab.com/malvinya/poleval-ner/raw/master/Malwina_Kowalczyk_INL_Projekt.pdf?inline=false)
## How to see training history in Colab
- Choose file with extension .ipynb from ```notebooks``` directory 
- Upload file to Colabolary
##How to recalculate evaluation results
- Select results file which you want to evaluate from ```results``` directory
- Open script ```/scripts/eval/poleval_ner_test.py```
- Put selected result filename in line 155:
```python
userfile = '../../results/results_crf_embeddings.json'
```
## How to run predictions on already trained model
- Select model which you want to use from ```models``` directory
- Open script ```process_test_data.py```
- Update lines 15-18 accordingly:
```python
RESULT_FILE = '../results/results_crf_embeddings.json'
MODEL_PATH = "../models/model_crf_embeddings.h5"
INDEX_TRANSFORMER_PATH = "../models/index_transformer_crf_embeddings.pkl"
IS_CRF_MODEL = True
```
## How to train model
- Make a copy of Jupyter notebook
- Upload notebook to Colabolatory
- Import files from /scripts directory into Google Colabolatory
- Mount Google Drive for 19901 user to get access to training set
- Modify notebook, modify NerModel params to your needs
- Run model, results will be save to 19901 user Google Drive

